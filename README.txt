This is an initial CMU FMS group analytic agent for the ASIST project,
computing a measure of cognitive load and one of the probability of
forgetting, written in Python. The agent proper is in one source file,
cmu_fms_analytic_agent.py.

It depends upon the details of semantically rich messages about
progress of the game that will be provided. Since the details of those
messages are not yet know some place holders are used instead. Four
possible topics that the agent subscribes to are defined in
cmu_fms_analytic_agent.py, as the constants. When the real topics are
known these will need to be updated. Most of these topics also require
some information to be provided in the message’s data slot. Since the
form of the data is also not yet known, small functions are provided
in cmu_fms_analytic_agent.py which extract it from a Python value
corresponding to a JSON object. Again, these functions will
undoubtedly need to be updated when the actual form of these objects
is known.

START_GAME_TOPIC: the intention is that this is seen when a new game
starts. It is used to reset the model. The message is not essential;
instead a new instance of the agent could be allocated for each game.

VICTIM_FIRST_SEEN: the intention is that this is seen whenever a victim is
first encountered. The data is expected to provide the victim’s id.

VICTIM_MOVED: the intention is that this is seen whenever a victim is
moved. The data is expected to provide the victim’s id.

VICTIM_TRIAGED: the intention is that this is seen whenever a victim is
triaged. The data is expected to provide the victim’s id.

The outputs from the agent are messages, again on a guessed topic with
a guessed structure. These, too, may need to be updated when the real
expectations of the game are better understood. For both the cognitive
load and the probability of forgetting a small dict (which will turn
into a JSON (sub-)object) is returned with two slots, value and
confidence. Since the project as a whole does not seem to have defined
what a “confidence” is, even syntactically, we’re taking our best
guess and returning a non-negative real. This, too, may eventually
need to be updated.

These measures are output after every victim specific message is
ingested, though based on the activations one second after that. Of
course, actual cognitive load possibly increases as time passes even
without meaningful events occurring, but this is only reflected when
an event occurs.

Since the actual experiment into which this needs to be plugged is not
yet available, as well as the messages it consumes not yet being
available, it has only be tested in vitro. A simple pytest test, in
test_agent.py, is provided. In addition to the one package described
in the requirements.txt file, for running this test pytest must also
be installed in the current environment. When it is, simply execute
“pytest” while cd’ed to this directory to run it. It is a de minimis
test, simply ensuring the code runs to completion and returns vaguely
sensible answers. This test also uses a stub implementation of the
asistagenthelper, dummy_asistagenthelper.py, to enable the test to be
run in vitro without the full experimental apparatus being available.

To use this agent “for real” in the experiment, the intention is that
an instance of CmuFmsAnalyticAgent is allocated, and then its start()
method is called.

Python version 3.8 or later is required to run the agent, as well as
the PyACTUp package described in the requirements.txt file.
