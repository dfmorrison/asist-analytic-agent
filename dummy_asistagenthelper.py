# Copyright 2022 Carnegie Mellon University

"""
A near vacuous ASISTAgentHelper definition to use for in vitro testing of the CMU FMS
group's initial analyitic agent.
"""

import logging
from datetime import datetime

class ASISTAgentHelper:

    def __init__(self, on_message_handler):
        self.logger = logging.getLogger(__name__)
        self.agent_name = "Dummy"
        self.config_folder = "DummyConfigFolder"
        self.last_response = None

    def get_logger(self):
        return self.logger

    def subscribe(self, topic, message_type=None, sub_type=None):
        pass

    def send_msg(self, topic, message_type, sub_type, sub_type_version, data=None, **ignore):
        self.last_response = data

    @staticmethod
    def generate_timestamp():
        return str(datetime.utcnow().isoformat()) + 'Z'
