#!/usr/bin/env python3

# Copyright 2022 Carnegie Mellon University

"""
Initial CMU FMS Group analytic agent for the ASIST project
"""

import json
import logging
import math
import os
import uuid

import pyactup

try:
    from asistagenthelper import ASISTAgentHelper
except ModuleNotFoundError:
    from dummy_asistagenthelper import ASISTAgentHelper

# The first two of these topics are completely made up, just place holders until we
# really know how the semantically rich data will be passed to us. The latter two are
# now believed to be real, though.
START_GAME_TOPIC = "command/init"
VICTIM_FIRST_SEEN = "observations/events/victim_first_seen"
VICTIM_MOVED = "observations/events/server/victim_placed"
VICTIM_TRIAGED = "observations/events/player/triage"

# Similarly, the following functions encapsulate how to get data out of the JSON objects
# that are delivered with the above topics, and again are made up place holders.

def parse_victim_first_seen(json):
    # returns one value, the victim's id
    return json["victim_id"]

def parse_victim_moved(json):
    # returns one value, the victim's id
    return json["victim_id"]

def parse_victim_triaged(json):
    # returns one value, the victim's id
    return json["victim_id"]

# Cognitive parameters
NOISE = 0.0
DECAY = 0.5
TEMPERATURE = 1
THRESHOLD = -1.0

CONFIDENCE_OFFSET = 0.25


class CmuFmsAnalyticAgent:

    def __init__(self):
        self.helper = ASISTAgentHelper(self.on_message)
        log_handler = logging.StreamHandler()
        log_handler.setFormatter(logging.Formatter("%(asctime)s | %(name)s | %(levelname)s — %(message)s"))
        self.helper.get_logger().setLevel(logging.INFO)
        self.helper.get_logger().addHandler(log_handler)
        self.logger = logging.getLogger(self.helper.agent_name)
        self.logger.setLevel(logging.INFO)
        self.logger.addHandler(log_handler)
        extra_path = os.path.join(self.helper.config_folder, 'extraInfo.json')
        self.extra_info = {}
        if os.path.exists(extra_path):
            with open(extra_path) as extra_file:
                self.extra_info = json.load(extra_file)
        if "default" not in self.extra_info.keys():
            self.extra_info["default"] = "I guess {0} is an okay role."
        for t in (START_GAME_TOPIC, VICTIM_FIRST_SEEN, VICTIM_MOVED, VICTIM_TRIAGED):
            self.helper.subscribe(t)
        self.memory = pyactup.Memory(noise=NOISE,
                                     decay=DECAY,
                                     temperature=TEMPERATURE,
                                     threshold=THRESHOLD,
                                     learning_time_increment=0)
        self.reset()

    def reset(self):
        self.memory.reset()
        self.last_time_seen = 0
        self.triaged_victims = set()

    def start(self):
        self.helper.set_agent_status(self.helper.STATUS_UP)
        self.logger.info("Starting Agent Loop on a separate thread.")
        self.helper.start_agent_loop_thread()
        self.logger.info("Agent is now running...")

    def update_time(self, data):
        self.last_time_seen = data["elapsed_milliseconds"] / 1000
        delta = self.last_time_seen - self.memory.time
        if delta < 0:
            self.helper.logger.error("time appears to be running backward")
            self.memory._time = self.last_time_seen
            delta = 0
        self.memory.advance(delta)
        return self.last_time_seen

    def on_message(self, topic, header, msg, data, mqtt_message):
        self.logger.debug(f"{topic} message received")
        if topic == START_GAME_TOPIC:
            self.reset()
            return
        elif topic == VICTIM_FIRST_SEEN:
            self.update_time(data)
            self.memory.learn(victim_id=parse_victim_first_seen(data))
        elif topic == VICTIM_MOVED:
            self.memory.learn(victim_id=parse_victim_moved(data))
        elif topic == VICTIM_TRIAGED:
            self.update_time(data)
            self.triaged_victims.add(parse_victim_triaged(data))
        else:
            return
        with self.memory.current_time:
            self.memory.advance()
            activations = [ a for a in [ c._activation() for c in self.memory.values()
                                         if c["victim_id"] not in self.triaged_victims ]
                            if a >= self.memory.threshold ]
        mid = self.exponentials(activations)
        low = self.exponentials(activations, CONFIDENCE_OFFSET)
        high = self.exponentials(activations, -CONFIDENCE_OFFSET)
        load = cognitive_load(mid)
        forgetting = probability_of_forgetting(mid)
        response_data = {
            "id": str(uuid.uuid4()),
            "agent": self.helper.agent_name,
            "created": self.helper.generate_timestamp(),
            "cognitive_load": {
                "value": load,
                "confidence": confidence(cognitive_load(low),
                                         load,
                                         cognitive_load(high)) },
            "probability_of_forgetting": {
                "value": forgetting,
                "confidence": confidence(probability_of_forgetting(low),
                                         forgetting,
                                         probability_of_forgetting(high)) }}
        self.helper.send_msg(f"agent/intervention/{self.helper.agent_name}",
                             "agent",
                             None,
                             "1.0",
                             timestamp=msg["timestamp"],
                             data=response_data)

    def exponentials(self, activations, offset=0):
        return [ math.exp((self.memory.threshold - (a + offset)) / self.memory._temperature)
                 for a in activations ]


def cognitive_load(exponentials):
    return sum(exponentials)

def probability_of_forgetting(exponentials):
    return 1 - math.prod(1 / (1 + e) for e in exponentials)

def confidence(low, middle, high):
    return (high - low) / middle
