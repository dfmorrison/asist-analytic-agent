# Copyright 2018-2021 Carnegie Mellon University

"""
A simiplistic, in vitro unit test of the CMU FMS analytic agent.
Just makes sure the code runs and produces values.
"""


import pytest

from cmu_fms_analytic_agent import *

def are_sensible_values(agent):
    assert 0 <= agent.helper.last_response["cognitive_load"]["value"]
    assert 0 <= agent.helper.last_response["probability_of_forgetting"]["value"] <= 1
    assert 0 <= agent.helper.last_response["cognitive_load"]["confidence"] <= 1
    assert 0 <= agent.helper.last_response["probability_of_forgetting"]["confidence"] <= 1

def test_agent():
    a = CmuFmsAnalyticAgent()
    # These tests will need to be modified whenever we know what the real form of the
    # semantically rich stuff we'll get to see is.
    a.on_message(VICTIM_FIRST_SEEN,
                 None,
                 { "timestamp": "sometime" },
                 { "elapsed_milliseconds": 1200, "victim_id": "123" },
                 None)
    are_sensible_values(a)
    a.on_message(VICTIM_MOVED,
                 None,
                 { "timestamp": "sometime" },
                 { "elapsed_milliseconds": 1450, "victim_id": "123" },
                 None)
    are_sensible_values(a)
    a.on_message(VICTIM_FIRST_SEEN,
                 None,
                 { "timestamp": "sometime" },
                 { "elapsed_milliseconds": 1600, "victim_id": "987" },
                 None)
    are_sensible_values(a)
    a.on_message(VICTIM_TRIAGED,
                 None,
                 { "timestamp": "sometime" },
                 { "elapsed_milliseconds": 2010, "victim_id": "987" },
                 None)
    are_sensible_values(a)
    a.on_message(VICTIM_FIRST_SEEN,
                 None,
                 { "timestamp": "sometime" },
                 { "elapsed_milliseconds": 1600, "victim_id": "343" },
                 None)
    are_sensible_values(a)
    a.on_message(VICTIM_TRIAGED,
                 None,
                 { "timestamp": "sometime" },
                 { "elapsed_milliseconds": 2010, "victim_id": "123" },
                 None)
    are_sensible_values(a)
